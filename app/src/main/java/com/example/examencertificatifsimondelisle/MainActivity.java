package com.example.examencertificatifsimondelisle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private Button button_connection;
    private TextView textView_highScore, textView_inscription;
    private EditText editText_password, editText_username;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_connection = (Button) findViewById(R.id.button_connection);
        textView_highScore = (TextView) findViewById(R.id.textView_highScore);
        textView_inscription = (TextView) findViewById(R.id.textView_inscription);
        editText_password = (EditText) findViewById(R.id.editText_password);
        editText_username = (EditText) findViewById(R.id.editText_username);

        button_connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);

                if(editText_username.getText().toString().equals(sharedPreferences.getString("username", "error"))
                        && editText_password.getText().toString().equals(sharedPreferences.getString("password", "error"))){
                    Intent intentGame = new Intent(MainActivity.this,Game.class);
                    startActivity(intentGame);
                }else
                {
                    Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
                }
            }
        });

        textView_inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSignUp = new Intent(MainActivity.this,SignIn.class);
                startActivity(intentSignUp);
            }
        });

        textView_highScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String score1 = sharedPreferences.getString("score", "error");
//                String user1 = sharedPreferences.getString("scoreUser", "error");
//
//                sharedPreferences.edit().putString("scoreUser",user1).commit();
//                sharedPreferences.edit().putString("score", score1).commit();
                Intent intentScore = new Intent(MainActivity.this,HighScore.class);
                startActivity(intentScore);
            }
        });

    }
}