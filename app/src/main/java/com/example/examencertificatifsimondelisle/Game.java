package com.example.examencertificatifsimondelisle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Game extends AppCompatActivity {

    private TextView textView_timer, textView_score;
    private Button button_point, button_retour;
    private int score = 0;
    private Handler hr;
    private Thread tr;
    private int counter = 11;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        button_point = (Button) findViewById(R.id.button_point);
        button_retour= (Button) findViewById(R.id.button_retour);

        textView_score = (TextView) findViewById(R.id.textView_score);
        textView_timer = (TextView) findViewById(R.id.textView_timer);

        button_retour.setVisibility(View.GONE);
        textView_score.setVisibility(View.GONE);

        hr = new Handler();
        tr = new Thread()
        {
            @Override
            public void run()
            {
                hr.postDelayed(tr,1000);
                counter--;
                textView_timer.setText(String.valueOf(counter));

                if(counter==0)
                {
                    textView_timer.setVisibility(View.GONE);
                    textView_score.setVisibility(View.VISIBLE);
                    button_point.setVisibility(View.GONE);
                    button_retour.setVisibility(View.VISIBLE);
                    hr.removeCallbacks(tr);
                }
            }
        };
        tr.run();

        button_retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
                String highS = sharedPreferences.getString("score", "error");
                int a = Integer.valueOf(highS);
                String sc = textView_score.getText().toString();
                int b = Integer.valueOf(sc);

                if(b > a){

                    sharedPreferences.edit().putString("score", textView_score.getText().toString()).apply();
                    String scoreUser =  sharedPreferences.getString("username", "error");
                    sharedPreferences.edit().putString("scoreUser", scoreUser).apply();
                    Intent intentRe = new Intent(Game.this,MainActivity.class);
                    startActivity(intentRe);
                    finish();
                }else{
                    Intent intentRe = new Intent(Game.this,MainActivity.class);
                    startActivity(intentRe);
                    finish();
                }

            }
        });

        button_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score++;
                textView_score.setText(String.valueOf(score));

            }
        });

    }
}